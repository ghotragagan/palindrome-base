package sheridan;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class PalindromeTest {

	@Test
	public void testIsPalindrome( ) {
		boolean isPalindrome = Palindrome.isPalindrome("Mom");
		assertTrue("Invalid Palindrome!", isPalindrome);
	}

	@Test
	public void testIsPalindromeNegative( ) {
		boolean isPalindrome = Palindrome.isPalindrome("gagandeep");
		assertFalse("Invalid Palindrome!", isPalindrome);
	}
	@Test
	public void testIsPalindromeBoundaryIn( ) {
		boolean isPalindrome = Palindrome.isPalindrome("po oop");
		assertTrue("Invalid Palindrome!", isPalindrome);
	}
	@Test
	public void testIsPalindromeBoundaryOut( ) {
		boolean isPalindrome = Palindrome.isPalindrome("detected");
		assertFalse("Invalid Palindrome!", isPalindrome);
	}	
	
}
